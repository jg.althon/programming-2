#include <conio.h>
#include <ctype.h>
#include <dos.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <windows.h>

COORD coord = {0, 0};
void firstScreen();
void SetColor(int ForgC);
void mainMenu();
void arrowHere(int, int);
void gotoxy(int, int);
void window();
void mainMenuWindow(int, int, int, int);
void box(int, int, int, int);
void hline(int, int, int);
void vline(int, int, int);

int main(void)
{
    firstScreen();
    mainMenu();
    registration();
    return 0;
}

/*************************
This function display the
first screen the user is
going to interact with
*************************/
void firstScreen()
{
    const char *s = "The Jamaican Gymnastic Competition";

    const int total_width = GetColumnWidth();
    const int s_width = strlen(s);
    const int field_width = (total_width - s_width) / 2 + s_width;

    for(int i = 0; i < 3; i++)
    {
        popupWindow();
        gotoxy(0,13);
        SetColor(2);
        printf("%*s\n", field_width, s);
        Sleep(500);
        system("cls");
        Sleep(500);

    }
    popupWindow();
    gotoxy(0,13);
    SetColor(2);
    printf("%*s\n", field_width, s);
    gotoxy(0, 17);
    printf(">> Press any key to continue...");

    _getch();
}

/*******************************
This function displays the main
menu for the user to select an
event
*******************************/
void mainMenu()
{
    #define high 6
    #define low 1
    system("cls");
    int position = 1;
    int keypressed = 0;

    while(keypressed != 13)
    {
        system("cls");
        SetColor(6);
        printf(">> Press the up or down arrow to navigate\n");
        printf(">> Press the enter key to select");
        SetColor(1);
        window(40, 4, 78, 6);
        window(40, 7, 78, 16);
        SetColor(6);
        gotoxy(55,5);
        printf("Main Menu");
        gotoxy(41, 9);
        arrowHere(1, position); printf("Registration\n");
        gotoxy(41, 10);
        arrowHere(2, position); printf("Participant Details\n");
        gotoxy(41, 11);
        arrowHere(3, position); printf("Participant Score Sheet\n");
        gotoxy(41, 12);
        arrowHere(4, position); printf("View Winners\n");
        gotoxy(41, 13);
        arrowHere(5, position); printf("Competition Report\n");
        gotoxy(41, 14);
        arrowHere(6, position); printf("Exit\n");

        keypressed = _getch();
        if(keypressed == 80 && position != high){
            position++;
        }
        else if(keypressed == 72 && position != low){
            position--;
        }
        else{
            position = position;
        }

        switch(position)
        {
        case 1:
            if(keypressed == 10){
                registration();
            }
            break;
        case 2:
            printf("Please select type of event:");
            break;
        case 3:
            printf("You have exited");
            break;
        case 4:
            printf("Testing\n");
            break;
        case 5:
            printf("Please select type of event:");
            break;
        case 6:
            printf("You have exited");
            break;
        }
    }
}

/************************
This function display the
menu for registration
************************/
void registration()
{
    #define up 3
    #define down 1
    system("cls");
    int position = 1;
    int keypressed = 0;

    while(keypressed != 13)
    {
        system("cls");
        SetColor(6);
        printf(">> Press the up or down arrow to navigate\n");
        printf(">> Press the enter key to select");
        SetColor(1);
        window(40, 4, 78, 6);
        window(40, 7, 78, 14);
        SetColor(6);
        gotoxy(53,5);
        printf("Registration");
        gotoxy(41, 9);
        printf("Please select type of competition:\n");
        gotoxy(41, 10);
        arrowHere(1, position); printf("Individual All-Around\n");
        gotoxy(41, 11);
        arrowHere(2, position); printf("Individual Events\n");
        gotoxy(41, 12);
        arrowHere(3, position); printf("Back to Menu\n");

         keypressed = _getch();
        if(keypressed == 80 && position != high){
            position++;
        }
        else if(keypressed == 72 && position != low){
            position--;
        }
        else{
            position = position;
        }

        switch(position)
        {
        case 1:
            printf("testing");
            break;
        case 2:
            printf("testing");
            break;
        case 3:
            if(keypressed == 10){
                mainMenu();
            }
            break;
        }
    }
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
/********************************************************************************************************/
//////////////////////////////////////////////////////////////////////////////////////////////////////////


/********************
This function center
the display of texts
********************/
#ifdef _WIN32
int GetColumnWidth()
{
    CONSOLE_SCREEN_BUFFER_INFO info;
    HANDLE out;

    if (!(out = GetStdHandle(STD_OUTPUT_HANDLE)) ||
        !GetConsoleScreenBufferInfo(out, &info))
        return 80;
    return info.dwSize.X;
}
#else
int GetColumnWidth() {return 80;}
#endif

/******************************
This function sets the display
color for a better UI
******************************/
void SetColor(int ForgC)
{
     WORD wColor;
     HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
     CONSOLE_SCREEN_BUFFER_INFO csbi;

     if(GetConsoleScreenBufferInfo(hStdOut, &csbi))
     {
          wColor = (csbi.wAttributes & 0xF0) + (ForgC & 0x0F);
          SetConsoleTextAttribute(hStdOut, wColor);
     }
     return;
}

/*****************************
This function allows the user
to use the arrow key to make
selection
*****************************/
void arrowHere(int realPosition, int arrowPosition)
{
    if(realPosition == arrowPosition){
        printf(">>  ");
    }
    else{
        printf("  ");
    }
}

/*********************************
This function gets the coordinates
and display the text to the
assigned coordinates
*********************************/
void gotoxy(int x,int y)
{
    coord.X=x;
    coord.Y=y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);
}

/**************************
This function display the
window for the firstScreen
function
**************************/
void popupWindow()
{
    SetColor(2);
    gotoxy(40, 11);
    for(int i = 1; i < 41; i++)
    {
        printf("\xB2");
    }
    gotoxy(40, 12);
    printf("\xB2\xB2");
    gotoxy(40, 14);
    printf("\xB2\xB2");
    gotoxy(40, 15);
    for(int i = 1; i < 41; i++)
    {
        printf("\xB2");
    }
    gotoxy(78, 12);
    printf("\xB2\xB2");
    gotoxy(78, 14);
    printf("\xB2\xB2");
}

/**************************
This function display the
window for the mainMenu
function
**************************/
void window(int x1,int y1,int x2, int y2)
{
    gotoxy(x2, y1);
    printf("%c", 187);
    gotoxy(x1, y2);
    printf("%c", 200);
    gotoxy(x2, y2);
    printf("%c", 188);
    gotoxy(x1, y1);
    printf("%c", 201);

    hline(x1+1, x2-1, y1);
    hline(x1+1, x2-1, y2);
    vline(y1+1, y2-1, x1);
    vline(y1+1, y2-1, x2);
}

void hline(int x1, int x2, int y)
{
    int i;
    for(i = x1; i <= x2; i++)
    {
        gotoxy(i, y);
        printf("%c", 205);
    }
}

void vline(int y1, int y2, int x)
{
    int i;
    for(i = y1; i <=y2; i++)
    {
        gotoxy(x, i);
        printf("%c", 186);
    }
}
